package net.freebytes.websocket.test;

import net.freebytes.websocket.api.WebsocketApi;
import net.freebytes.websocket.common.dto.WsMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 千里明月
 * @date 2021/8/8 17:57
 */
@RestController
public class TestController {
    @Autowired
    private WebsocketApi websocketApi;

    @GetMapping("sent-to-all")
    public void sentToAllUsers() throws Exception {
        websocketApi.sentToAllUsers("/new-record", "", "收到新消息");
    }

    @GetMapping("sent-to-users")
    public void sentToUsers() throws Exception {
        List<String> users = new ArrayList();
        users.add("1");
        users.add("2");
        users.add("3");
        WsMessage wsMessage = new WsMessage("收到新消息", users);
        websocketApi.sentToUsers("/new-record", "", wsMessage);
    }

}
