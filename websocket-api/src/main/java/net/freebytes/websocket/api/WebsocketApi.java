package net.freebytes.websocket.api;

import net.freebytes.websocket.common.dto.WsMessage;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * websocketApi
 *
 * @author 千里明月
 * @date 2021/8/8
 **/
@FeignClient(name = "ws-api", url = "${api.url.ws-server:localhost:8080}")
public interface WebsocketApi {

    /**
     * 推送websocket
     *
     * @param prefix 前缀
     * @param item   地址
     * @param obj    对象信息
     * @throws Exception 异常
     */
    @PostMapping("/api/freebytes/ws/sent-to-all")
    void sentToAllUsers(@RequestParam String prefix, @RequestParam String item, @RequestBody Object obj) throws Exception;

    /**
     * 指定用户推送websocket
     *
     * @param prefix 前缀
     * @param item   地址
     * @param msg    对象信息
     * @throws Exception 异常
     */
    @PostMapping("/api/freebytes/ws/sent-to-user")
    void sentToUsers(@RequestParam String prefix, @RequestParam String item, @RequestBody WsMessage msg) throws Exception;
}
