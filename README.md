# single-websocket

#### 介绍
一个可独立部署的websocket服务，提供基于feign的api，可在多服务架构中通过@FeignClient远程调用以发送消息给前端。

#### 原创
出自明月工作室 http://www.freebytes.net

#### 软件架构
软件架构说明

1. websocket-common    存放实体类
2. websocket-starter         websocket单体服务，可直接运行
3. websocket-api               供其他服务调用的api，可直接在maven上引入其依赖，并通过@Autowire注入WebsocketApi使用
4. websocket-test              测试服务，可直接运行

#### 安装教程

1.  运行websocket-starter
2.  运行websocket-test
3.  打开http://localhost:8080/index.html
4.  访问http://localhost:8081/sent-to-users

#### 使用说明

1.  部署websocket-starter

2.  在你的项目中引入maven依赖websocket-api
        <dependency>
            <groupId>net.freebytes</groupId>
            <artifactId>websoket-api</artifactId>
            <version>1.0</version>
        </dependency>
        
3.  注入WebsocketApi
     @Autowired
      private WebsocketApi websocketApi;

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
