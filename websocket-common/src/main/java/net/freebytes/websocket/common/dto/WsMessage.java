package net.freebytes.websocket.common.dto;

import java.util.List;

/**
 * 消息体
 *
 * @author 千里明月
 * @date 2021/8/8
 **/
public class WsMessage {
    /**
     * 消息内容
     */
    private Object message;
    /**
     * 用户列表 ids
     */
    private List<String> users;

    public WsMessage() {
    }

    public WsMessage(Object message, List<String> users) {
        this.message = message;
        this.users = users;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}
